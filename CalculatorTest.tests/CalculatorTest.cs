using NUnit.Framework;
using CalculatorKata;
using System;

namespace CalculatorTest.tests
{
    [TestFixture]
    public class CalculatorTest
    {
        private Calculator calculator;
        [SetUp]
        public void Setup()
        {
            this.calculator = new Calculator();
        }

        [Test]
        [TestCase(0, "")]
        public void GIVEN_EmptyString_WHEN_AddingNumbers_RETURN0(int expected, string numbers)
        {
            Assert.AreEqual(expected, this.calculator.Add(numbers));
        }

        [TestCase(1, "1")]
        [TestCase(2, "2")]
        public void GIVEN_String1_WHEN_AddingNumbers_RETURN1(int expected, string numbers)
        {
            Assert.AreEqual(expected, this.calculator.Add(numbers));
        }

        [TestCase(3, "1,2")]
        public void GIVEN_CommaSeparatedStringNumberWHEN_AddingNumbers_RETURN_Sum(int expected, string numbers)
        {
            Assert.AreEqual(expected, this.calculator.Add(numbers));
        }

        [TestCase(29, "1,2,7,3,6,9,1")]
        [TestCase(11, "1,1,2,3,4")]
        public void GIVEN_CommaSeparator_WHEN_AddingMoreNumbers_RETURN_Sum(int expected, string numbers)
        {
            Assert.AreEqual(expected, this.calculator.Add(numbers));
        }

        [TestCase(6, "1\n2,3")]
        public void GIVEN_NewLinesSeparators_WHEN_AddingNumbers_RETURN_Sum(int expected, string numbers)
        {
            Assert.AreEqual(expected, this.calculator.Add(numbers));
        }

        [TestCase(3, "//;\n1;2")]
        public void GIVEN_CustomDelimiters_WHEN_AddingNumbers_RETURN_SumOfNumbers(int expected, string numbers)
        {
            Assert.AreEqual(expected, this.calculator.Add(numbers));
        }

        [TestCase("-2", "1,-2")]
        public void GIVEN_NegativeNumbers_WHEN_AddingNumbers_RETURN_ThrowException(string expectedNegatives, string numbers)
        {

            var exception = Assert.Throws<Exception>(() => this.calculator.Add(numbers));

            Assert.AreEqual("Negatives not allowed : " + expectedNegatives, exception.Message);
        }

        [TestCase(2, "2,1001")]
        public void GIVEN_NumbersAbove1000_WHEN_AddingTwoPlus1001_RETURN_2(int expected, string numbers)
        {
            Assert.AreEqual(expected, this.calculator.Add(numbers));
        }

        [TestCase(6, "//***\n1***2***3")]
        public void GIVEN_CustomDelimiter_WHEN_AddingNumbers_RETURN_Sum(int expected, string numbers)
        {
            Assert.AreEqual(expected, this.calculator.Add(numbers));
        }

        [TestCase(6, "//[*][%]\n1*2%3")]
        public void GIVEN_MultipleCustomDelimiter_WHEN_AddingNumbers_RETURN_Sum(int expected, string numbers)
        {
            Assert.AreEqual(expected, this.calculator.Add(numbers));
        }
    }
}